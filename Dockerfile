FROM alpine

RUN apk update && apk add gdb tzdata

RUN rm -rf /var/spool/cron /etc/crontabs || /bin/true

VOLUME ["/var/spool/cron", "/etc/cron.d"]

COPY ./src/crond /usr/local/bin/
COPY ./src/crontab /usr/local/bin/
COPY ./src/cronnext /usr/local/bin/

RUN mkdir /usr/lib/debug

COPY ./src/cron*.debug /usr/lib/debug/

ARG TZ="Europe/Athens"
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime

CMD ["crond", "-s"]
